public class Image : LightNode
{
    public string Href { get; }
    public IImagesStrategy Strategy { private get; set; }

    public Image(string href)
    {
        Href = href;
    }

    public override string Render()
    {
        return Strategy?.Execute(Href);
    }
}