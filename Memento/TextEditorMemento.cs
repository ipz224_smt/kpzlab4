public class TextEditorMemento : IMemento
{

    public TextEditorMemento(TextDocument state)
    {
        this.State = state;
    }

    public TextDocument State { get; private set; }
}