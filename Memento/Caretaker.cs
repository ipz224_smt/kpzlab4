
public class Caretaker
{
    private TextEditor textEditor;
    private Stack<IMemento> history;

    public Caretaker(TextEditor textEditor)
    {
        this.textEditor = textEditor;
        this.history = new();
    }

    public void Save()
    {
        history.Push(textEditor.Save());
    }

    public bool Restore()
    {
        history.TryPop(out IMemento result);

        if (result is null)
            return false;

        textEditor.Restore(result);

        return true;
    }
}