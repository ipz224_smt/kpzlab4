public class TextDocument
{
    public TextDocument(string content)
    {
        Content = content;
    }

    public string Content { get; private set; }
}