
public class TextEditor
{
    public TextDocument CurrentDocument { get; private set; }

    public TextEditor(string initialContent)
    {
        CurrentDocument = new TextDocument(initialContent);
    }

    public void Edit(string content)
    {
        CurrentDocument = new TextDocument(content);
    }

    public IMemento Save()
    {
        return new TextEditorMemento(CurrentDocument);
    }

    public void Restore(IMemento memento)
    {
        switch (memento)
        {
            case TextEditorMemento correntMement:
                CurrentDocument = correntMement.State;
                return;
        }

        throw new InvalidCastException("Invalid type of memento object");
    }
}
