namespace Lab4.Mediator
{
  class Runway : BaseComponent
  {
    private readonly object _lock = new();
    public string Id { get; private set; }
    private bool isBusyWithAircraft;
    public bool IsBusyWithAircraft
    {
      set
      {
        lock (_lock)
        {
          if (value && isBusyWithAircraft)
          {
            throw new Exception("Runway is already busy");
          }

          isBusyWithAircraft = value;
        }
      }
    }

    public Runway(string id)
    {
      this.Id = id;
    }

    public bool CheckIsActive()
    {
      return !isBusyWithAircraft;
    }

    public void HighLightRed()
    {
      Console.WriteLine($"Runway {this.Id} is busy!");
    }

    public void HighLightGreen()
    {
      Console.WriteLine($"Runway {this.Id} is free!");
    }
  }
}