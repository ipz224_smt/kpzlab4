namespace Lab4.Mediator;
public interface IMediator
{
    public void Notify(Request request);
}