namespace Lab4.Mediator;
public record Request(string command, object requestData);