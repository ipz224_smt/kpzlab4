namespace Lab4.Mediator
{
  class Aircraft : BaseComponent
  {
    private string runWayName;
    public string Name;
    public bool IsTakingOff { get; set; }
    public Aircraft(string name, int size)
    {
      this.Name = name;
    }
    public void Land(string runWayName)
    {

      Console.WriteLine($"Aircraft {this.Name} is landing.");
      Console.WriteLine($"Checking runway.");

      try
      {
        _mediator.Notify(new Request("BusyRunway", runWayName));
        this.runWayName = runWayName;
        Console.WriteLine($"Aircraft {this.Name} has landed.");
      }
      catch
      {
        Console.WriteLine($"Could not land, the runway is busy.");
      }

    }
    public void TakeOff()
    {
      
      if (runWayName is null)
      {
        Console.WriteLine($"Aircraft {this.Name} is already taking off.");
        return;
      }

      Console.WriteLine($"Aircraft {this.Name} is taking off.");

      _mediator.Notify(new Request("TakeOffRunway", runWayName));

      Console.WriteLine($"Aircraft {this.Name} has took off.");
    }
  }
}