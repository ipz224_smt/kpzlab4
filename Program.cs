﻿using Lab4.ChainOfResponsibility;
using Lab4.Mediator;

var handler1 = new AskQuestionHandler();
var handler2 = new RequestFeatureHandler();
var handler3 = new RequestCustomizationHandler();
var handler4 = new ReportProblemHandler();

string response = null;

handler1.SetNext(handler2).SetNext(handler3).SetNext(handler4);

Console.WriteLine("\nChain of responsibility\n");

while (response is null)
{

    Console.WriteLine("Welcome to User Support System");
    Console.WriteLine("Please choose an option:");
    Console.WriteLine("1. Ask a question");
    Console.WriteLine("2. Request a feature");
    Console.WriteLine("3. Request a customization");
    Console.WriteLine("4. Report a problem");

    int result = 0;

    if (!int.TryParse(Console.ReadLine(), out result))
    {
        continue;
    }

    response = handler1.Handle(result);

}

Console.WriteLine(response);


Console.WriteLine("\nMediator\n");

var runways = new Dictionary<string, Runway>();
runways.Add("R1", new Runway("R1"));
runways.Add("R2", new Runway("R2"));
runways.Add("R3", new Runway("R3"));
runways.Add("R4", new Runway("R4"));

var aircrafts = new Dictionary<string, Aircraft>();
aircrafts.Add("A1", new Aircraft("A1", 12));
aircrafts.Add("A2", new Aircraft("A3", 12));
aircrafts.Add("A3", new Aircraft("A3", 12));
aircrafts.Add("A4", new Aircraft("A4", 12));

var commandCenter = new CommandCentre(runways, aircrafts);

aircrafts["A1"].Land("R1");
aircrafts["A2"].Land("R1");

Console.WriteLine("\nObserver\n");

LightTextNode node = new LightTextNode("Hello");
node.AddEventListener("afterRender", new SubscriberAfterRender());
node.AddEventListener("beforeRender", new SubscriberBeforeRender());
Console.WriteLine($"Render data : {node.Render()}");
node.NotifyAfterRender();

Console.WriteLine("\nStrategy\n");
var image = new Image("image.png");
image.Strategy = new ImageFileSystemLoadStrategy();
Console.WriteLine(image.Render());
image.Strategy = new ImageNetworkLoadStrategy();
Console.WriteLine(image.Render());

Console.WriteLine("\nMemento\n");

var textEditor = new TextEditor("start content");

var caretaker = new Caretaker(textEditor);

caretaker.Save();

textEditor.Edit("Hello world");

caretaker.Save();

textEditor.Edit("Hello wonderful world");

caretaker.Save();

textEditor.Edit("Hello wonderful world :))))");

Console.WriteLine("Current content: " + textEditor.CurrentDocument.Content);

caretaker.Restore();

Console.WriteLine("Current content: " + textEditor.CurrentDocument.Content);

caretaker.Restore();

Console.WriteLine("Current content: " + textEditor.CurrentDocument.Content);

caretaker.Restore();
caretaker.Restore();
caretaker.Restore();
caretaker.Restore();
caretaker.Restore();

Console.WriteLine("Current content: " + textEditor.CurrentDocument.Content);