namespace Lab4.ChainOfResponsibility;

public abstract class BaseUserSupportHandler : IHandler<string, int>
{

    private IHandler<string, int> next;

    public IHandler<string, int> SetNext(IHandler<string, int> next)
    {
        this.next = next;
        return next;
    }

    public virtual string Handle(int request)
    {
        return this.next?.Handle(request);
    }

}
