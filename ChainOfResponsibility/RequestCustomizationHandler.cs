namespace Lab4.ChainOfResponsibility;
public class RequestCustomizationHandler : BaseUserSupportHandler
{

    public override string Handle(int request)
    {
        if (request == 3)
        {
            return $"{nameof(RequestCustomizationHandler)} handled the request";
        }

        return base.Handle(request);
    }
}