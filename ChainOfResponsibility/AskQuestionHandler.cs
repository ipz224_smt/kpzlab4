namespace Lab4.ChainOfResponsibility;

public class AskQuestionHandler : BaseUserSupportHandler
{

    public override string Handle(int request)
    {
        if (request == 1)
        {
            return $"{nameof(AskQuestionHandler)} handled the request";
        }

        return base.Handle(request);
    }
}