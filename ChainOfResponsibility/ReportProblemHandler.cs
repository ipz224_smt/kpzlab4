
namespace Lab4.ChainOfResponsibility;

public class ReportProblemHandler : BaseUserSupportHandler
{

    public override string Handle(int request)
    {
        if (request == 4)
        {
            return $"{nameof(ReportProblemHandler)} handled the request";
        }

        return base.Handle(request);
    }
}