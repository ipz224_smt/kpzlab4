using System.Collections.Concurrent;

public abstract class LightNode
{
    protected ConcurrentDictionary<string, List<ISubscriber>> events;

    protected LightNode()
    {
        events = new();
    }

    public string AddEventListener(string eventName, ISubscriber subscriber)
    {

        try
        {
            events[eventName].Add(subscriber);
        }
        catch
        {
            events[eventName] = new() { subscriber };
        }

        return $"{eventName} has been added";
    }
    public string RemoveEventListener(string eventName, ISubscriber subscriber)
    {

        try
        {
            events[eventName].Remove(subscriber);
            return $"{eventName} has been removed";
        }
        catch
        {
            return $"There is no {eventName}";
        }
    }

    protected void NotifyBeforeRender()
    {
        events.TryGetValue("beforeRender", out List<ISubscriber> subscribers);
        subscribers?.ForEach(el => el.Notify(this));
    }

    public void NotifyAfterRender()
    {
        events.TryGetValue("afterRender", out List<ISubscriber> subscribers);
        subscribers?.ForEach(el => el.Notify(this));
    }

    public abstract string Render();
}