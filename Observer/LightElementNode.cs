using System.Text;
public class LightElementNode : LightNode
{
    public string TagName { get; }
    public string DisplayType { get; }
    public bool IsClosed { get; }
    public IEnumerable<string> Classes { get; }
    public IEnumerable<LightNode> Children { get; }
    public LightElementNode(string tagName, string displayType, bool isClosed, List<string> classes, List<LightNode> children)
    {
        TagName = tagName;
        DisplayType = displayType;
        IsClosed = isClosed;
        Classes = classes;
        Children = children;
    }

    public string OuterHTML
    {
        get
        {
            StringBuilder html = new StringBuilder();
            html.Append($"<{TagName}");

            if (Classes is not null && Classes.Any())
            {
                html.Append(" class=\"");
                html.Append(string.Join(" ", Classes));
                html.Append('"');
            }

            if (IsClosed)
            {
                html.Append('>');
                if (Children is not null && Children.Any())
                {
                    foreach (var child in Children)
                    {
                        html.Append(child.Render());
                    }
                }
                html.Append($"</{TagName}>");
            }
            else
            {
                html.Append("/>");
            }

            return html.ToString();
        }
    }

    public string InnerHTML
    {
        get
        {
            var html = new StringBuilder();
            foreach (var child in Children)
            {
                html.Append(child.Render());
            }
            return html.ToString();
        }
    }

    public override string Render()
    {
        NotifyBeforeRender();
        var data = OuterHTML;
        return data;
    }

    public override string ToString()
    {
        return Render();
    }
}
