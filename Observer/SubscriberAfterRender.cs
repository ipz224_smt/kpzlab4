public class SubscriberAfterRender : ISubscriber
{
    public void Notify(object eventData)
    {
        Console.WriteLine($"type of element after render: {eventData.GetType()}");
    }
}