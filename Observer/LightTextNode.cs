
public class LightTextNode : LightNode
{
    public string Text { get; }
    public LightTextNode(string text)
    {
        Text = text;
    }

    public override string Render()
    {
        NotifyBeforeRender();
        var text = Text;
        return text;
    }

    public override string ToString()
    {
        return Render();
    }
}