public interface ISubscriber
{
    void Notify(object eventData);
}